" vimplug
call plug#begin('~/.vim/plugged')
Plug 'tpope/vim-sensible'
" ui
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
" languages
Plug 'vim-ruby/vim-ruby'
Plug 'plasticboy/vim-markdown'
Plug 'bfrg/vim-cpp-modern'
call plug#end()

" general options
set showtabline=2
set number

" airline options
let g:airline#extensions#tabline#enabled = 1
let g:airline_theme='term'


export EDITOR=/usr/bin/nvim
export VISUAL=/usr/bin/nvim
export BROWSER=/usr/bin/vivaldi-stable
export PATH="$PATH:$(ruby -e 'puts Gem.user_dir')/bin"
export PG_OF_PATH=/home/maya/OF

# zgen load / plugins

source "${HOME}/.zgen/zgen.zsh"

if ! zgen saved; then
	zgen oh-my-zsh
	
	zgen load willghatch/zsh-saneopt
	zgen load zdharma/fast-syntax-highlighting
	zgen oh-my-zsh plugins/git
	zgen oh-my-zsh plugins/sudo
	zgen oh-my-zsh plugins/command-not-found
	zgen load joow/youtube-dl

	zgen oh-my-zsh themes/cypher

	zgen save
fi

# aliases

alias stx='startx'

alias yayy='yay -S --noconfirm'
alias yayu='yay -Syu --noconfirm'

alias -g cp="cp -rv"

alias -g vim="nvim"
alias -g vi="nvim"
alias -g nv="nvim"

alias pingt="ping archlinux.org"
